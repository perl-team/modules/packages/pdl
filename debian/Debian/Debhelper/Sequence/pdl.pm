# pdl.pm - debhelper addon for running dh_pdl
#
# Copyright 2022, Bas Couwenberg <sebastic@debian.org>
#
# This program is free software, you can redistribute it and/or modify it
# under the same terms as Perl itself.

use strict;
use warnings;

use Debian::Debhelper::Dh_Lib;

insert_after("dh_perl", "dh_pdl");

1;
