Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PDL
Upstream-Contact: PDL Developers <pdl-general@lists.sourceforge.net>
Source: https://metacpan.org/release/PDL

Files: *
Copyright:               1997-2006, the contributors named in the individual files
                              2011, Christopher Marshall
                         1997-2010, Karl Glazebrook, Craig DeForest, and Doug Burke
                              2010, Derek Lamb <lambd@users.sourceforge.net>
                              2006, Andres Jordan <ajordan@eso.org>
                              2002, Dov Grobgeld <dov@imagic.weizmann.ac.il>
                              2001, NetGroup A/S
                         1997-1998, Tuomas J. Lukka <lukka@husc.harvard.edu>
                              1998, Raphael Manfredi <Raphael_Manfredi@hp.com>
                              1997, Karl Glazebrook and Alison Offer Real code
                              1997, Christian Soeller <c.soeller@auckland.ac.nz>
 1973, 1984, 1987-1989, 1991, 1995, Stephen L. Moshier
Comment: From the COPYING file:
 .
 Inline documentation in any module files (pod format) and documentation
 files (.pod files) in this distribution are additionally protected by
 the following statement:
 .
 Permission is granted for verbatim copying (and formatting) of this
 documentation as part of the PDL distribution. Permission is granted to
 freely distribute verbatim copies of this documentation only if
 the following conditions are met: 1. that the copyright notice remains
 intact 2. the original authors' names are clearly displayed, 3. that
 any changes made to the documentation outside the official PDL
 distribution (as released by the current release manager) are clearly
 marked as such and 4. That this copyright notice is distributed with
 the copied version so that it may be easily found.
 .
 All the files in the distribution should have a copyright notice
 according to the following template:
 .
       Copyright (C) 199X Author1, Author2.
       All rights reserved. There is no warranty. You are allowed
       to redistribute this software / documentation as described
       in the file COPYING in the PDL distribution.
 .
 In addition, the following disclaimers apply:
 .
 THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
 FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
 OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
 PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
 EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS
 WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
 ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 .
 IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
 WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
 REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR
 DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM
 (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED
 INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF
 THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER
 OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
License: Artistic or GPL-1+

Files: lib/PDL/IO/Dumper.pm
Copyright: 2002, Craig DeForest
Comment: This code may be distributed under the same terms as Perl itself
 (license available at https://ww.perl.org).  Copying, reverse
 engineering, distribution, and modification are explicitly allowed so
 long as this notice is preserved intact and modified versions are
 clearly marked as such.
 .
 This package comes with NO WARRANTY.
License: Artistic or GPL-1+

Files: lib/PDL/FFT/fftn.c
       lib/PDL/FFT/fftn.h
Copyright: 1995, 1997, Mark Olesen <olesen@me.QueensU.CA>
                       Queen's Univ at Kingston (Canada)
License: fftn
 Permission to use, copy, modify, and distribute this software for
 any purpose without fee is hereby granted, provided that this
 entire notice is included in all copies of any software which is
 or includes a copy or modification of this software and in all
 copies of the supporting documentation for such software.
 .
 THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR
 IMPLIED WARRANTY.  IN PARTICULAR, NEITHER THE AUTHOR NOR QUEEN'S
 UNIVERSITY AT KINGSTON MAKES ANY REPRESENTATION OR WARRANTY OF ANY
 KIND CONCERNING THE MERCHANTABILITY OF THIS SOFTWARE OR ITS
 FITNESS FOR ANY PARTICULAR PURPOSE.
 .
 All of which is to say that you can do what you like with this
 source code provided you don't try to sell it as your own and you
 include an unaltered copy of this message (including the
 copyright).
 .
 It is also implicitly understood that bug fixes and improvements
 should make their way back to the general Internet community so
 that everyone benefits.

Files: lib/PDL/Image2D/rotate.c
       lib/PDL/ImageRGB/ppm_quant.c
Copyright: 1989, 1991 Jef Poskanzer
License: Jef-Poskanzer
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted, provided
 that the above copyright notice appear in all copies and that both that
 copyright notice and this permission notice appear in supporting
 documentation.  This software is provided "as is" without express or
 implied warranty.

Files: debian/*
Copyright: 1998-1999, John Lapeyre <lapeyre@physics.arizona.edu>
           1999-2001, Raul Miller <moth@debian.org>
           2003-2014, Henning Glawe <glaweh@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
