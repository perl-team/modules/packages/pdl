#!/usr/bin/perl
use strict;
use warnings;
my $hot=0;
while (<>) {
    last if (/^END test verbose/);
    if (/^BEGIN test verbose/) {
        $hot = 1;
        next;
    }
    print if ($hot);
}
