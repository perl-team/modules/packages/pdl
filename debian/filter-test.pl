#!/usr/bin/perl
use strict;
use warnings;

my @cmdline=('make','TEST_VERBOSE=1','LC_ALL=C');
push @cmdline,"LDFLAGS=$ENV{LDFLAGS}" if (defined $ENV{LDFLAGS});
push @cmdline,'test';
push @cmdline,'2>&1';

my $cmdline=join(' ',@cmdline);

open(my $fh_test,'-|',$cmdline);
my $hot=0;
my $last_enter_dir;
my $debug = 0;
print "BEGIN test verbose\n";
while (<$fh_test>) {
	if (/^make\[\d+\]: Entering directory/) {
		$last_enter_dir=$_;
		$hot=0;
		next;
	}
	if (/\"test_harness\(/) {
		$hot=2;
		if ($debug) {
			print "ADDHOT: " . $last_enter_dir;
		} else {
			print $last_enter_dir;
		}
	}
	if ($hot) {
		$hot-- if ($hot < 2);
		if ($debug) {
			print "   HOT: " . $_;
		} else {
			print;
		}
		if (/^Result:\s*(?:PASS|FAIL)/) {
			$hot--;
		}
	} elsif ($debug) {
		print "  COLD: " . $_ unless ($hot);
	}
}
print "END test verbose\n";
my $status=close($fh_test);
exit($status ? 0 : 1);
