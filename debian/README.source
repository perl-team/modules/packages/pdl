To update an already-existing dev-release patch, do this:

    PREV=2.068_03
    NEW=2.068_04
    quilt push 0001-update-to-upstream-dev-release-$PREV.patch
    quilt add $(cd ../pdl-code/; git diff --name-only $PREV..$NEW)
    (cd ../pdl-code/; git diff $PREV..$NEW)|patch -p1
    quilt refresh
    quilt pop -a
    dch --team 'Update dev-release patch to latest dev-release'
    debcommit -a # now rename patch, edit description, update patches/series
    time (perlbrew off; gbp buildpackage --git-pbuilder --git-dist=sid)

To incorporate an upstream fix-patch (BUT `gbp pq` might be better):

    COMMIT=xxx
    git format-patch -1 -N -k -o debian/patches $COMMIT
    FILE=0001-blah.patch
    basename $FILE >> debian/patches/series
    quilt push -a && quilt refresh

To update from upstream (see also
https://debian-gis-team.pages.debian.net/policy/packaging.html#git-build-package,
https://www.debian.org/doc/manuals/developers-reference/best-pkging-practices.en.html#bpp-debian-changelog):

    gbp pull # updates local repo in case another maintainer packaged the previous
    gbp import-orig --uscan
    # check patches for previous release
    (perlbrew off; gbp buildpackage --git-pbuilder) # test packaging
    gbp dch -R # SET unstable if done
    # check changelog, and commit - cf debcommit -a
    # NOT `gbp push`, see https://debian-gis-team.pages.debian.net/policy/packaging.html#git-push-to-salsa

To refresh patches, repeat quilt push; quilt refresh, then after: quilt pop -a

To go from UNRELEASED to experimental:
    dch -rD experimental
    append the ~exp1 suffix to package revision
